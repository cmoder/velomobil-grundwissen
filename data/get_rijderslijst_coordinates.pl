#!/usr/bin/perl -F'\t' -al

use LWP::Simple;
use URI::Escape;
use JSON;

# locations of dealers (regex)
@dealers = ("huefingen", "rapperswil", "murphy", "chasnais", "le mans", "dornbirn", "stenløse",
	"wölfersheim", "hannover", "^elen\$", "gu[eé]rard", "dueville", "nijmegen", "dronten",
	"duitsland", "^gent\$", "heist op den berg", "winterswijk", "taklax", "eckernförde");

# get location (column 12), remove whitespace, convert to lowercase
$loc = ($F[11] =~ s/^\s*(.*?)\s*$/\L\1/r);

# increase number for this location (if given)
$loc and $loc_num{$loc}++;

# get country for this location (if given), remove whitespace, convert to lowercase
$loc and $F[12] and $loc_country{$loc} //= ($F[12] =~ s/^\s*(.*?)\s*$/\L\1/r);

END {
	# iterate over all locations
	for $l (sort keys %loc_num) {

		# request coordinates
		$c = decode_json(get("http://nominatim.openstreetmap.org/search?format=json&dedupe=1&limit=1&q=" . uri_escape($l) . ($loc_country{$l} ? "&country=" . uri_escape($loc_country{$l}) : "") ));

		# skip if no coordinates have been found
		$c->[0] or next;

		# write a table: lon, lat, number of velomobiles, dealer or user, comment
		print join "\t", (map { $c->[0]{$_} } ("lon", "lat")), $loc_num{$l}, scalar(grep { $l =~ $_ } @dealers) ? "dealer" : "user", "# $l, $loc_country{$l}";
	}
}
