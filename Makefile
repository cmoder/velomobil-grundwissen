# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = python3 `which sphinx-build`
SPHINXPROJ    = Velomobil-Grundwissen
SOURCEDIR     = .
BUILDDIR      = _build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile images

# generate all images:
# * extract velomobile orders from velomobileworld HTML orderlist (https://www.velomobileworld.com/de/auftraege/)
# * plot all Matplotlib diagrams
# * create GraphViz diagram
# * before, add timeline to GraphViz diagram:
#   * extract all years (in parentheses) from nodes
#   * save the node names in hash %a with key = year and value = list of node names with that year
#   * after the nodes (i.e. when the first "_future" key is encountered and not a comment): create sorted list of years (in array @years)
#   * add the node names that contain "_future" to the same hash %a (key = "future")
#   * => only if there is no node entry for the last year; otherwise: don’t store the future node, and remove it from the line
#   * save all lines of file in array @lines
#   * at the end of the input file: output all lines
#   * add "_future" nodes to the last recorded year (move the nodes from the key "future" to the key with the last year)
#   * output pairs of years where "//// TIMELINE" is written in file (= arrow to each year from its previous year)
#   * output a list of "_future" nodes (with shape="none" and no label) directly afterwards
#   * output each year with its nodes and "rank=same" where "//// RANKS" is written in the file
images:
	cd images && \
	\
	../data/velomobileworld_orderlist_csv.pl ../data/velomobiel_intercitybike_production.csv ../data/velomobileworld_orderlist_2022-09-05.html > ../data/velomobileworld_orderlist.csv && \
	\
	python3 ../data/plot_diagrams.py && \
	\
	../data/plot_maps.sh && \
	\
	for lang in de en; do \
	perl -ale \
	'	/\((\d{4})\)<br/ and push(@{$$a{$$1}}, $$F[0]);'\
	'	!@years and /\w+_future/ and !/\/\/.*_future/ and @years = sort { $$a <=> $$b } grep { !/_future/ } keys %a; '\
	'	/(\w+)\s*->\s*(\w+_future)\W/ and ( !grep { /$$1/ } @{$$a{$$years[-1]}} and push(@{$$a{future}}, $$2) or s/\s*->\s*\w+_future// );'\
	'	push(@lines, $$_);'\
	'END {'\
	'	map {'\
	'		$$_ = join "\n", map { "\t$$years[$$_-1] -> $$years[$$_];" } (1..$$#years);'\
	'		$$_ .= "\n\n\tnode [ shape=\"none\" label=\"\" ];\n\n";'\
	'		$$_ .= join "\n", map { "\t$$_;" } @{$$a{future}};'\
	'	} grep { m!//// TIMELINE$$! } @lines; '\
	'	push(@{$$a{$$years[-1]}}, @{delete $$a{future}});'\
	'	map { $$_ = join "\n", map { "\t{ rank=same; " . join( " ", $$_, @{$$a{$$_}} ) . "; }" } @years } grep { m!//// RANKS$$! } @lines; '\
	'	print for @lines; '\
	'}' ../data/vm_history_$$lang.dot | dot -Tsvg:cairo > vm_history_$$lang.svg; \
	done


# images in image-src:
#
# $ rawtherapee-cli -p images-src/Hosen_Alpha7_DF_P1066995.rw2.pp3 -j70 -js3 -o images/Hosen_Alpha7_DF_P1066995.jpg -c images-src/Hosen_Alpha7_DF_P1066995.rw2
# $ rawtherapee-cli -p images-src/Liegeradtage_Ornbau_2021_P1067003.RW2.pp3 -j70 -js3 -o images-src/Liegeradtage_Ornbau_2021_P1067003.jpg -c images-src/Liegeradtage_Ornbau_2021_P1067003.RW2
#
# $ jpegoptim --verbose --strip-all --size=1500 --stdout images-src/Velomobil_Walchensee_P1056440.jpg > images-src/Velomobil_Walchensee_P1056440_jpegoptim.jpg
# $ jpegoptim --verbose --strip-all --size=1500 --stdout images-src/Liegeradtage_Ornbau_2021_P1067003.jpg > images-src/Liegeradtage_Ornbau_2021_P1067003_jpegoptim.jpg
# $ jpegoptim --verbose --strip-all --size=1000 --dest=images/ images-src/NACA-Duct_MilanSL_P1056821.jpg
#
# $ inkscape --export-pdf=images/Titelbild_Velomobil_Walchensee_P1056440_Laksaman_de.pdf images-src/Titelbild_Velomobil_Walchensee_P1056440_Laksaman_de.svg
# $ inkscape --export-pdf= images/Liegeradtage_Ornbau_2021_P1067003.pdf images-src/Liegeradtage_Ornbau_2021_P1067003.svg


# EPUB: change MathJax to imgmath
epub: Makefile
	$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O) -D extensions=sphinx.ext.imgmath,sphinx.ext.extlinks,sphinx.ext.autosectionlabel,sphinxcontrib.rsvgconverter

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

